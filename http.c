#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <curl/curl.h>
#include "http.h"

char has_initiated = 0;

/**
 * @brief Resizable buffer to collect http response body
 */
typedef struct
{
    char* buf;
    size_t size;
} memory;

/**
 * @brief Make new mem object of given size
 * 
 * @param size 
 * @return memory* 
 */
memory* make_mem(size_t size)
{
    memory* mem = malloc(sizeof(memory));
    mem->buf = malloc(size + 1);
    mem->size = size;
    return mem;
}

/**
 * @brief Free memory object allocations
 * 
 * @param mem 
 */
void free_mem(memory *mem)
{
    free(mem->buf);
    free(mem);
}

/**
 * @brief Grow buffer and append new chunk
 * 
 * @param contents new chunk
 * @param sz chunk size
 * @param nmemb :shrug:
 * @param ctx memory object
 * @return size_t
 */
size_t grow_buffer(void* contents, size_t sz, size_t nmemb, void* ctx)
{
    size_t realsize = sz * nmemb;
    memory* mem = (memory*) ctx;
    char* ptr = realloc(mem->buf, mem->size + realsize + 1);
    if (!ptr)
    {
        fprintf(stderr, "not enough memory (realloc returned NULL)\n");
        exit(EXIT_FAILURE);
    }
    mem->buf = ptr;
    memcpy(&(mem->buf[mem->size]), contents, realsize);
    mem->size += realsize;
    mem->buf[mem->size] = 0x0;
    return realsize;
}

/**
 * @brief Make a new CURL handle with a growable memory to save response body
 * 
 * @param url 
 * @return CURL* 
 */
CURL* make_handle(char* url)
{
    CURL* handle = curl_easy_init();

    /* Important: use HTTP2 over HTTPS */
    // curl_easy_setopt(handle, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2TLS);
    curl_easy_setopt(handle, CURLOPT_URL, url);

    /* buffer body */
    memory* mem = make_mem(0);
    curl_easy_setopt(handle, CURLOPT_PRIVATE, (void*) mem);
    curl_easy_setopt(handle, CURLOPT_WRITEDATA, (void*) mem);
    curl_easy_setopt(handle, CURLOPT_WRITEFUNCTION, grow_buffer);

    /* For completeness */
    curl_easy_setopt(handle, CURLOPT_ACCEPT_ENCODING, "utf-8");
    curl_easy_setopt(handle, CURLOPT_TIMEOUT, 5L);
    curl_easy_setopt(handle, CURLOPT_FOLLOWLOCATION, 1L);
    curl_easy_setopt(handle, CURLOPT_MAXREDIRS, 10L);
    curl_easy_setopt(handle, CURLOPT_CONNECTTIMEOUT, 2L);
    // curl_easy_setopt(handle, CURLOPT_COOKIEFILE, "/tmp/dockdl.cookie");
    curl_easy_setopt(handle, CURLOPT_FILETIME, 1L);
    curl_easy_setopt(handle, CURLOPT_USERAGENT, "dockdl/v0.1.0");
    curl_easy_setopt(handle, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
    curl_easy_setopt(handle, CURLOPT_UNRESTRICTED_AUTH, 1L);
    curl_easy_setopt(handle, CURLOPT_PROXYAUTH, CURLAUTH_ANY);
    curl_easy_setopt(handle, CURLOPT_EXPECT_100_TIMEOUT_MS, 0L);
    return handle;
}

/**
 * @brief Send http request to given url and return response body
 * 
 * @param url 
 * @return char* 
 */
char* http_get(char* url, http_header_bag_t* header_bag)
{
    CURL* handle = make_handle(url);

    struct curl_slist* headers_slist = NULL;
    if (header_bag && header_bag->size > 0)
    {
        for (int i = 0; i < header_bag->size; i++)
        {
            http_header_t* header = header_bag->headers[i];
            char* h = malloc(strlen(header->name) + strlen(header->value) + 3);
            sprintf(h, "%s: %s\0", header->name, header->value);
            headers_slist = curl_slist_append(headers_slist, h);
            if (!headers_slist)
                printf("curl_slist_append failed");
        }
        curl_easy_setopt(handle, CURLOPT_HTTPHEADER, headers_slist);
    }

    CURLcode res = curl_easy_perform(handle);
    if (res != CURLE_OK)
    {
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(res));
        curl_easy_cleanup(handle);
        return NULL;
    }
    

    memory* mem;
    curl_easy_getinfo(handle, CURLINFO_PRIVATE, &mem);
    curl_easy_cleanup(handle);
    if (headers_slist) curl_slist_free_all(headers_slist);

    char* body = malloc(mem->size + 1);
    memcpy(body, mem->buf, mem->size);
    body[mem->size] = 0x00;
    free_mem(mem);

    return body;
}

/**
 * @brief Initiate curl globally
 */
void http_init()
{
    if (has_initiated) {
        fprintf(stderr, "http has been initiated already");
        return;
    }

    curl_global_init(CURL_GLOBAL_ALL);
    has_initiated = 1;
}

/**
 * @brief Cleanup curl globally
 */
void http_destroy()
{
    if (!has_initiated) {
        fprintf(stderr, "http has not been initiated");
        return;
    }

    curl_global_cleanup();
    has_initiated = 0;
}

http_header_bag_t* make_http_header_bag()
{
    http_header_bag_t* bag = malloc(sizeof(http_header_bag_t));
    bag->size = 0;
    return bag;
}

http_header_t* make_http_header(char* name, char* value)
{
    http_header_t* header = malloc(sizeof(http_header_t));

    size_t name_len = strlen(name);
    header->name = malloc(name_len + 1);
    strncpy(header->name, name, name_len);
    header->name[name_len] = 0x0;

    size_t value_len = strlen(value);
    header->value = malloc(value_len + 1);
    strncpy(header->value, value, value_len);
    header->value[value_len] = 0x0;

    return header;
}

void add_http_header(http_header_bag_t* bag, http_header_t* header)
{
    int new_bag_size = bag->size + 1;
    if (new_bag_size > MAX_ALLOWED_HEADERS)
    {
        fprintf(stderr, "maximum number of header cannot exceed %d",
                MAX_ALLOWED_HEADERS);
        exit(10);
    }

    bag->headers[bag->size] = header;
    bag->size++;
}
