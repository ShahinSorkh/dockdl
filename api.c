#include <string.h>

#include <jansson.h>
#include "http.h"
#include "api.h"

#define JSON_DUMPS_FLAGS 516 /* JSON_ENCODE_ANY | JSON_INDENT(4) */

#define DOCKER_AUTH_URL_FORMAT "https://auth.docker.io/token?service=registry.docker.io&scope=repository:%s:pull\0"
#define DOCKER_AUTH_URL_SIZE 79

#define DOCKER_MANIFEST_URL_FORMAT "https://registry-1.docker.io/v2/%s/manifests/%s\0"
#define DOCKER_MANIFEST_URL_SIZE 44

#define HEADER_AUTHORIZATION_NAME "Authorization"
#define HEADER_AUTHORIZATION_FORMAT "Bearer %s\0"
#define HEADER_AUTHORIZATION_SIZE 8

#define HEADER_ACCEPT_NAME "Accept"
#define HEADER_ACCEPT_V1_JSON "application/vnd.docker.distribution.manifest.v1+json"
#define HEADER_ACCEPT_V2_JSON "application/vnd.docker.distribution.manifest.v2+json"
#define HEADER_ACCEPT_LIST_V2_JSON "application/vnd.docker.distribution.manifest.list.v2+json"


/**
 * @brief Get the token from http response
 * 
 * @param resp 
 * @return const char* 
 */
const char* get_token_from_response(char* resp)
{
    json_t* root;
    json_error_t error;

    root = json_loads(resp, 0, &error);
    free(resp);

    if (!root)
    {
        fprintf(stderr, "error: on line %d: %s\n",
                error.line, error.text);
        return NULL;
    }

    if (!json_is_object(root))
    {
        fprintf(stderr, "error: root is not an object\n");
        fprintf(stderr, "%s\n", json_dumps(root, JSON_DUMPS_FLAGS));
        json_decref(root);
        return NULL;
    }

    json_t* token = json_object_get(root, "token");
    if (!json_is_string(token))
    {
        fprintf(stderr, "error: token is not string\n");
        fprintf(stderr, "%s\n", json_dumps(token, JSON_DUMPS_FLAGS));
        json_decref(root);
        return NULL;
    }

    const char* token_str = json_string_value(token);
    char* auth_token = malloc(strlen(token_str));
    strcpy(auth_token, token_str);
    json_decref(root);

    return auth_token;
}

/**
 * @brief Get the auth token for the given image
 * 
 * @param image 
 * @return const char* 
 */
const char* get_auth_token(char* image)
{
    size_t url_size = DOCKER_AUTH_URL_SIZE + strlen(image);
    char* auth_url = malloc(url_size);
    sprintf(auth_url, DOCKER_AUTH_URL_FORMAT, image);

    char* response = http_get(auth_url, NULL);
    if (!response) return NULL;

    return get_token_from_response(response);
}

char* get_manifest(const char* token, const char* image, const char* digest)
{
    char* bearer_token = malloc(HEADER_AUTHORIZATION_SIZE + strlen(token));
    sprintf(bearer_token, HEADER_AUTHORIZATION_FORMAT, token);

    http_header_bag_t* header_bag = make_http_header_bag();
    add_http_header(header_bag,
                    make_http_header(HEADER_AUTHORIZATION_NAME, bearer_token));
    add_http_header(header_bag,
                    make_http_header(HEADER_ACCEPT_NAME, HEADER_ACCEPT_V2_JSON));
    add_http_header(header_bag,
                    make_http_header(HEADER_ACCEPT_NAME, HEADER_ACCEPT_LIST_V2_JSON));
    add_http_header(header_bag,
                    make_http_header(HEADER_ACCEPT_NAME, HEADER_ACCEPT_V1_JSON));

    char *manifest_url = malloc(DOCKER_MANIFEST_URL_SIZE + strlen(image) + strlen(digest));
    sprintf(manifest_url, DOCKER_MANIFEST_URL_FORMAT, image, digest);
    
    char* resp = http_get(manifest_url, header_bag);
    if (!resp)
    {
        fprintf(stderr, "Failed on getting manifest of %s with token %s\n",
                manifest_url, token);
        return NULL;
    }
    return resp;
}
