#ifndef _DOCKDL_API_H
#define _DOCKDL_API_H

const char* get_auth_token(char* image);
char* get_manifest(const char* token, const char* image, const char* digest);

#endif
