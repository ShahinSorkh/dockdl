#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "api.h"
#include "http.h"

int main(int argc, char **argv) {
  http_init();

  char *image_tag = "library/ubuntu";
  const char *image_digest = "latest";
  const char *token = get_auth_token(image_tag);

  if (!token)
    return EXIT_FAILURE;

  char *manifest = get_manifest(token, image_tag, image_digest);
  if (!manifest)
    return EXIT_FAILURE;

  printf("%s\n", manifest);

  http_destroy();
  return EXIT_SUCCESS;
}
