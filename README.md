# Docker Downloader

## Build

You would need `libcurl` and `libjansson` to build this project.

```sh
$ mkdir build && cd build
$ cmake .. && make
```
