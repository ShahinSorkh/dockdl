#ifndef _DOCKDL_HTTP_H
#define _DOCKDL_HTTP_H

#define MAX_ALLOWED_HEADERS 10

typedef struct
{
    char* name;
    char* value;
} http_header_t;

typedef struct
{
    http_header_t* headers[MAX_ALLOWED_HEADERS];
    size_t size;
} http_header_bag_t;

void http_init();
void http_destroy();
char* http_get(char* url, http_header_bag_t* header_bag);

http_header_bag_t* make_http_header_bag();
http_header_t* make_http_header(char* name, char* value);
void add_http_header(http_header_bag_t* bag, http_header_t* header);

#endif

